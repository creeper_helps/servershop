package com.avalon.token;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.avalon.servershop.ServerShop;

public class TokenUtils {
	
	  public ServerShop plugin;
	  public static File home;
	  
		public TokenUtils(ServerShop instance) {
			plugin = instance;
		}


	public static boolean hasAccount(String p) {
		  File configlist = new File(home.getAbsolutePath() + File.separator + "token.yml");
		  FileConfiguration cfg = YamlConfiguration.loadConfiguration(configlist);
		  if (cfg.isSet(p)) {
			  return true;
		  }
		  return false;
	  }
	  
	  public static int getToken(String p) {
		  if (hasAccount(p)) {
			  File configlist = new File(home.getAbsolutePath() + File.separator + "token.yml");
			  FileConfiguration cfg = YamlConfiguration.loadConfiguration(configlist);	  
		  return cfg.getInt(p);
		  } else {
			  return 0;
		  }
	  }
	  
	  public static void giveToken(String p, int amount) {
		  File configlist = new File(home.getAbsolutePath() + File.separator + "token.yml");
		  FileConfiguration cfg = YamlConfiguration.loadConfiguration(configlist);
		  if (hasAccount(p)) {
			  int currentToken = cfg.getInt(p);
			  cfg.set(p, (currentToken + amount));
			  try {
				cfg.save(configlist);
			} catch (IOException e) {
				e.printStackTrace();
			}
			  return;
		  } else {
			  cfg.set(p,amount);
			  try {
				cfg.save(configlist);
			} catch (IOException e) {
				e.printStackTrace();
			}
			  return;
		  }
	  }
	  
	  public static void setToken(String p, int amount) {
		  File configlist = new File(home.getAbsolutePath() + File.separator + "token.yml");
		  FileConfiguration cfg = YamlConfiguration.loadConfiguration(configlist);
		  cfg.set(p, amount);
		  try {
				cfg.save(configlist);
			} catch (IOException e) {
				e.printStackTrace();
			}
		  return;
	  }
	  
	  public static boolean takeToken(String p, int amount) {
		  File configlist = new File(home.getAbsolutePath() + File.separator + "token.yml");
		  FileConfiguration cfg = YamlConfiguration.loadConfiguration(configlist);
		  if (hasAccount(p)) {
			  int currentToken = cfg.getInt(p);
			  int newbalance = currentToken - amount;
			  if (newbalance >= 0 ) {
			  cfg.set(p, (currentToken - amount));
			  } else {
				  return false;
			  }
			  try {
					cfg.save(configlist);
				} catch (IOException e) {
					e.printStackTrace();
				}
			  return true;
		  } else {
			  return false;
		  }
	  }
}
