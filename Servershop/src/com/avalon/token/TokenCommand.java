package com.avalon.token;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import com.avalon.mysql.TokenMySQL;
import com.avalon.servershop.ServerShop;
import com.avalon.utils.Lang;

public class TokenCommand implements CommandExecutor {
	
	
	public ServerShop plugin;
	public TokenCommand(ServerShop instance) {
		plugin = instance;
	}
	

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd,
			String commandLabel, String[] args) {
		    	
		    	
		    		
		    if (args.length == 0) {
		    	if (sender instanceof Player) {
		    		Player p = (Player) sender;
		    	if (plugin.MySQL_E)	{
		    		p.sendMessage(Lang.TOKEN_BALANCE.toString().replace("%token%", "" + TokenMySQL.getToken(p.getName())));
		    		return true;
		    	}
		    	p.sendMessage(Lang.TOKEN_BALANCE.toString().replace("%token%", "" + TokenUtils.getToken(p.getName())));
		    	return true;
		    } else {
		    	respond(sender, ChatColor.RED + "This command cannot be executed from console");
		    	return true;
		    }
		    }
		    
		    if (args.length == 1) {
		    	if (sender instanceof Player) {
		    		Player p = (Player) sender;
		    		if (args[0].equalsIgnoreCase("top") && plugin.MySQL_E) {
		    			TokenMySQL.sendTopTen(p);
		    		}
		    	return true;
		    } else {
		    	respond(sender, ChatColor.RED + "This command cannot be executed from console");
		    	return true;
		    }
		    }
		    if (args.length == 2) {
		    	 if (args[0].equalsIgnoreCase("balance")) {
		    		 
		    		 if (!checkPerm(sender, "tokens.balance.others")) {
		    			 respond(sender, Lang.NO_PERMISSION.toString());
		    			 return true;
		    		 }
		    		 
		    		 if (Bukkit.getPlayer(args[1]) != null) {
		    		if (plugin.MySQL_E) {
		    			respond(sender, Lang.TOKEN_BALANCE_OTHERS.toString().replace("%target%", Bukkit.getPlayer(args[1]).getName()).replace("%token%", ""+TokenMySQL.getToken(Bukkit.getPlayer(args[1]).getName())));
		    			return true;
		    		}
		    		respond(sender, Lang.TOKEN_BALANCE_OTHERS.toString().replace("%target%", Bukkit.getPlayer(args[1]).getName()).replace("%token%", ""+TokenUtils.getToken(Bukkit.getPlayer(args[1]).getName())));
		    		return true;
		    		 } else {
		    			 respond(sender, Lang.TOKEN_WARNING.toString());
		    			 if (plugin.MySQL_E) {
		    				 respond(sender, Lang.TOKEN_BALANCE_OTHERS.toString().replace("%target%", args[1]).replace("%token%", ""+TokenMySQL.getToken(args[1])));
		    				 return true;
		    			 }
		    			 respond(sender, Lang.TOKEN_BALANCE_OTHERS.toString().replace("%target%", Bukkit.getPlayer(args[1]).getName()).replace("%token%", ""+TokenUtils.getToken(Bukkit.getPlayer(args[1]).getName())));
		    			 return true;
		    		 }
		    	}
		    	 
		    	 
		    	 
		    	 else {
		    		sendhelp(sender);
		    	}
		    	
		    }
		    
		    
		    
		    if (args.length == 3) {
		    	if (args[0].equalsIgnoreCase("add") || args[0].equalsIgnoreCase("give")) {
		    		
		    		if (!checkPerm(sender, "tokens.add")) {
		    			respond(sender, Lang.NO_PERMISSION.toString());
		    			 return true;
		    		 }
		    		
		    		if (Bukkit.getPlayer(args[1]) != null) {
		    		if (isInt(args[2])) {
			    		if (plugin.MySQL_E) {
			    			TokenMySQL.giveToken(Bukkit.getPlayer(args[1]).getName(), Integer.parseInt(args[2]));
			    			respond(sender, Lang.TOKEN_ADD.toString().replace("%token%", args[2]).replace("%player%", Bukkit.getPlayer(args[1]).getName()));
			    			return true;
			    		}
			    		TokenUtils.giveToken(Bukkit.getPlayer(args[1]).getName(), Integer.parseInt(args[2]));
			    		respond(sender, Lang.TOKEN_ADD.toString().replace("%token%", args[2]).replace("%player%", Bukkit.getPlayer(args[1]).getName()));
			    		return true;
		    		}
		    		} else {
		    			 
				    		if (isInt(args[2])) {
				    			respond(sender, Lang.TOKEN_WARNING.toString());
				    			if (plugin.MySQL_E) {
				    				TokenMySQL.giveToken(args[1], Integer.parseInt(args[2]));
				    				respond(sender, Lang.TOKEN_ADD.toString().replace("%token%", args[2]).replace("%player%", args[1]));
				    				return true;
				    			}
					    		
					    		TokenUtils.giveToken(args[1], Integer.parseInt(args[2]));
				    			respond(sender, Lang.TOKEN_ADD.toString().replace("%token%", args[2]).replace("%player%", args[1]));
				    			return true;
				    		}
		    		 }
		    	}
		    	
		    	else if (args[0].equalsIgnoreCase("take")) {
		    		
		    		if (!checkPerm(sender, "tokens.take")) {
		    			respond(sender, Lang.NO_PERMISSION.toString());
		    			 return false;
		    		 }
		    		
		    		
		    		if (Bukkit.getPlayer(args[1]) != null) {
		    		if (isInt(args[2])) {
		    			
		    			if (plugin.MySQL_E) {
		    				if (TokenMySQL.takeToken(Bukkit.getPlayer(args[1]).getName(), Integer.parseInt(args[2]))) {
				    			respond(sender, Lang.TOKEN_REMOVE.toString().replace("%token%", args[2]).replace("%player%", Bukkit.getPlayer(args[1]).getName()));
				    		return true;
				    		} else {
				    			respond(sender,Lang.TOKEN_TARGET_NOT_ENOUGH.toString().replace("%player%", Bukkit.getPlayer(args[1]).getName()));
				    			return true;
				    		}
		    			}
		    			
		    			
		    		if (TokenUtils.takeToken(Bukkit.getPlayer(args[1]).getName(), Integer.parseInt(args[2]))) {
		    			respond(sender, Lang.TOKEN_REMOVE.toString().replace("%token%", args[2]).replace("%player%", Bukkit.getPlayer(args[1]).getName()));
			    		return true;
			    		} else {
			    			respond(sender,Lang.TOKEN_TARGET_NOT_ENOUGH.toString().replace("%player%", Bukkit.getPlayer(args[1]).getName()));
			    			return true;
			    		}
		    			
		    		}
		    	} else {
		    		if (isInt(args[2])) {
		    			respond(sender, Lang.TOKEN_WARNING.toString());
		    			
		    			if (plugin.MySQL_E) {
		    				if (TokenMySQL.takeToken(args[1], Integer.parseInt(args[2]))) {
				    			respond(sender, Lang.TOKEN_REMOVE.toString().replace("%token%", args[2]).replace("%player%", args[1]));
				    		return true;
				    		}
				    		 else {
				    			 respond(sender,Lang.TOKEN_TARGET_NOT_ENOUGH.toString().replace("%player%", args[1]));
				    			 return true;
					    		}
		    			}
		    			
		    			
			    		if (TokenUtils.takeToken(args[1], Integer.parseInt(args[2]))) {
			    			if (TokenMySQL.takeToken(args[1], Integer.parseInt(args[2]))) {
				    			respond(sender, Lang.TOKEN_REMOVE.toString().replace("%token%", args[2]).replace("%player%", args[1]));
				    		return true;
				    		}
				    		 else {
				    			 respond(sender,Lang.TOKEN_TARGET_NOT_ENOUGH.toString().replace("%player%", args[1]));
					    		}
			    		}
		    			
		    		}
	    		 }
		    	}
		    	
		    	
		    	else if (args[0].equalsIgnoreCase("send")) {
		    		
		    		if (!checkPerm(sender, "tokens.send")) {
		    			respond(sender, Lang.NO_PERMISSION.toString());
		    			 return false;
		    		 }
		    		if (sender instanceof Player) {
		    			Player giver = (Player) sender;
		    			Player receiver = Bukkit.getPlayer(args[1]);

		    			if (receiver != null) {
		    				if (isInt(args[2])) {
		    					
		    					if (plugin.MySQL_E) {
		    						if (TokenMySQL.takeToken(giver.getName(), Integer.parseInt(args[2]))) {
			    						TokenMySQL.giveToken(receiver.getName(), Integer.parseInt(args[2]));
			    						giver.sendMessage(Lang.TOKEN_SEND.toString().replace("%token%", args[2]).replace("%player%", receiver.getName()));
			    						receiver.sendMessage(Lang.TOKEN_RECEIVE.toString().replace("%token%", args[2]).replace("%player%", receiver.getName()));
			    						return true;
			    					} else {
			    						giver.sendMessage(Lang.TOKEN_NOT_ENOUGH.toString());
			    						return true;
			    					}
		    					}
		    					if (TokenUtils.takeToken(giver.getName(), Integer.parseInt(args[2]))) {
		    						TokenUtils.giveToken(receiver.getName(), Integer.parseInt(args[2]));
		    						giver.sendMessage(Lang.TOKEN_SEND.toString().replace("%token%", args[2]).replace("%player%", receiver.getName()));
		    						receiver.sendMessage(Lang.TOKEN_RECEIVE.toString().replace("%token%", args[2]).replace("%player%", receiver.getName()));
		    						return true;
		    					} else {
		    						giver.sendMessage(Lang.TOKEN_NOT_ENOUGH.toString());
		    						return true;
		    					}
		    				} else {
		    					giver.sendMessage(ChatColor.RED + "This is not a valid amount.");
		    					return true;
		    				}
		    				
		    			} else {
		    				giver.sendMessage(ChatColor.DARK_RED + args[1] + " is not online!");
		    				return true;
		    			}
		    			
		    			
		    			
		    			
		    			
		    		} else {
		    			respond(sender, ChatColor.RED + "You must be a Player to use this command.");
		    		}
		    	}
		    	
		    	else if (args[0].equalsIgnoreCase("set")) {
		    		
		    		if (!checkPerm(sender, "tokens.set")) {
		    			respond(sender, Lang.NO_PERMISSION.toString());
		    			 return false;
		    		 }
		    		
		    		
		    		if (Bukkit.getPlayer(args[1]) != null) {
		    		if (isInt(args[2])) {
		    			if (plugin.MySQL_E) {
		    				TokenMySQL.setToken(Bukkit.getPlayer(args[1]).getName(), Integer.parseInt(args[2]));
				    		respond(sender,Lang.TOKEN_SET.toString().replace("%player%", Bukkit.getPlayer(args[1]).getName()).replace("%token%", args[2]));
				    		return true;
		    			}
		    		TokenUtils.setToken(Bukkit.getPlayer(args[1]).getName(), Integer.parseInt(args[2]));
		    		respond(sender,Lang.TOKEN_SET.toString().replace("%player%", Bukkit.getPlayer(args[1]).getName()).replace("%token%", args[2]));
		    		return true;
		    		}
		    	} else {
		    		if (isInt(args[2])) {
		    			respond(sender, Lang.TOKEN_WARNING.toString());
		    			if (plugin.MySQL_E) {
		    				TokenMySQL.setToken(args[1], Integer.parseInt(args[2]));
				    		respond(sender,Lang.TOKEN_SET.toString().replace("%player%", args[1]).replace("%token%", args[2]));
				    		return true;
		    				
		    			}
			    		TokenUtils.setToken(args[1], Integer.parseInt(args[2]));
			    		respond(sender,Lang.TOKEN_SET.toString().replace("%player%", args[1]).replace("%token%", args[2]));
			    		return true;
			    		}
	    		 }
		    }
		    	}

		    return true;
	}
	
	
	private boolean isInt(String s) {
		try {
			int i = Integer.parseInt(s);
            if (i < 0) {
                return false;
            }
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	private void respond(CommandSender sender, String text) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			p.sendMessage(text);
			return;
		} else {
			Bukkit.getConsoleSender().sendMessage(text);
			return;
		}
	}
	
	private void sendhelp(CommandSender sender) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			p.sendMessage(ChatColor.RED + "Command not found!");
			p.sendMessage(ChatColor.AQUA + "/tokens - " + ChatColor.YELLOW + " Displays your token balance.");
			p.sendMessage(ChatColor.AQUA + "/tokens balance <player> - " + ChatColor.YELLOW + " Displays Token balance of <player>");
			p.sendMessage(ChatColor.AQUA + "/tokens add <player> <amount> - " + ChatColor.YELLOW + " Adds <amount> of tokens to <players> balance. ");
			p.sendMessage(ChatColor.AQUA + "/tokens take <player> <amount>  - " + ChatColor.YELLOW + " Takes <amount> of tokens from <players> balance.");
			p.sendMessage(ChatColor.AQUA + "/tokens set <player> <amount>  - " + ChatColor.YELLOW + " Sets <players> balance to <amount>");
			p.sendMessage(ChatColor.AQUA + "/tokens send <player> <amount>  - " + ChatColor.YELLOW + " Send <player> a total of <amount> tokens from your balance.");
			return;
		} else {
			sender.sendMessage(ChatColor.RED + "Command not found!");
			sender.sendMessage(ChatColor.AQUA + "/tokens balance <player> - " + ChatColor.YELLOW + " Displays Token balance of <player>");
			sender.sendMessage(ChatColor.AQUA + "/tokens add <player> <amount> - " + ChatColor.YELLOW + " Adds <amount> of tokens to <players> balance. ");
			sender.sendMessage(ChatColor.AQUA + "/tokens take <player> <amount>  - " + ChatColor.YELLOW + " Takes <amount> of tokens from <players> balance.");
			sender.sendMessage(ChatColor.AQUA + "/tokens set <player> <amount>  - " + ChatColor.YELLOW + " Sets <players> balance to <amount>");
			return;
		}
	}
	
	private boolean checkPerm(CommandSender sender, String permissionnode) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (p.hasPermission(permissionnode)) {
				return true;
			}
			else {
				respond(sender, Lang.NO_PERMISSION.toString());
				return false;
			}
		}
		else if (sender instanceof ConsoleCommandSender) {
			return true;
		}
		return false;
	}
}
