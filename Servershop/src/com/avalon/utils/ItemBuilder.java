package com.avalon.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.MaterialData;
 
public class ItemBuilder {
		
	private final ItemStack	                           is;
	
	/**
	 * Inits the builder with the given {@link Material}
	 * 
	 * @param mat
	 *            the {@link Material} to start the builder from
	 */
	public ItemBuilder(final Material mat) {
		is = new ItemStack(mat);
	}
	
	/**
	 * Inits the builder with the given {@link ItemStack}
	 * 
	 * @param is the {@link ItemStack} to start the builder from
	 */
	public ItemBuilder(final ItemStack is) {
		this.is = is;
	}
	
	/**
	 * Changes the amount of the {@link ItemStack}
	 * 
	 * @param amount the new amount to set
	 * @return this builder for chaining
	 */
	public ItemBuilder amount(final int amount) {
		is.setAmount(amount);
		return this;
	}
	
	/**
	 * Changes the display name of the {@link ItemStack}
	 * 
	 * @param name the new display name to set
	 * @return this builder for chaining
	 */
	public ItemBuilder name(final String name) {
		final ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(name);
		is.setItemMeta(meta);
		return this;
	}
	
	/**
	 * Adds a new line to the lore of the {@link ItemStack}
	 * 
	 * @param text the new line to add
	 * @return this builder for chaining
	 */
	public ItemBuilder lore(final String name) {
		final ItemMeta meta = is.getItemMeta();
		List<String> lore = meta.getLore();
		if (lore == null) {
			lore = new ArrayList<String>();
		}
		lore.add(name);
		meta.setLore(lore);
		is.setItemMeta(meta);
		return this;
	}
	
	/**
	 * Adds a ArrayList to the lore of the {@link ItemStack}
	 * 
	 * @param text the new line to add
	 * @return this builder for chaining
	 */
	public ItemBuilder lore(final List<String> lore) {
		final ItemMeta meta = is.getItemMeta();
		meta.setLore(lore);
		is.setItemMeta(meta);
		return this;
	}
	
	/**
	 * Make {@link ItemStack} unbreakable
	 * 
	 * @param unbreakable unbreakable status.
	 * @return this builder for chaining
	 */
	public ItemBuilder setUnbreakable(final boolean unbreakable) {
		final ItemMeta meta = is.getItemMeta();
		meta.spigot().setUnbreakable(unbreakable);
		is.setItemMeta(meta);
		return this;
	}
	
	/**
	 * Changes the durability of the {@link ItemStack}
	 * 
	 * @param durability the new durability to set
	 * @return this builder for chaining
	 */
	public ItemBuilder durability(final int durability) {
		is.setDurability((short) durability);
		return this;
	}
	
	/**
	 * Changes the data of the {@link ItemStack}
	 * 
	 * @param data the new data to set
	 * @return this builder for chaining
	 */
	@SuppressWarnings("deprecation")
	public ItemBuilder data(final int data) {
		is.setData(new MaterialData(is.getType(), (byte) data));
		return this;
	}
	
	/**
	 * Adds an {@link Enchantment} with the given level to the {@link ItemStack}
	 * 
	 * @param enchantment
	 *            the enchantment to add
	 * @param level the level of the enchantment
	 * @return this builder for chaining
	 */
	public ItemBuilder enchantment(final Enchantment enchantment, final int level) {
		is.addUnsafeEnchantment(enchantment, level);
		return this;
	}
	
	/**
	 * Adds an {@link Enchantment} with the level 1 to the {@link ItemStack}
	 * 
	 * @param enchantment the enchantment to add
	 * @return this builder for chaining
	 */
	public ItemBuilder enchantment(final Enchantment enchantment) {
		is.addUnsafeEnchantment(enchantment, 1);
		return this;
	}
	
	/**
	 * Changes the {@link Material} of the {@link ItemStack}
	 * 
	 * @param data the new material to set
	 * @return this builder for chaining
	 */
	public ItemBuilder type(final Material material) {
		is.setType(material);
		return this;
	}
	
	/**
	 * Clears the lore of the {@link ItemStack}
	 * 
	 * @return this builder for chaining
	 */
	public ItemBuilder clearLore() {
		final ItemMeta meta = is.getItemMeta();
		meta.setLore(new ArrayList<String>());
		is.setItemMeta(meta);
		return this;
	}
	
	/**
	 * Clears the list of {@link Enchantment}s of the {@link ItemStack}
	 * 
	 * @return this builder for chaining
	 */
	public ItemBuilder clearEnchantments() {
		for (final Enchantment e : is.getEnchantments().keySet()) {
			is.removeEnchantment(e);
		}
		return this;
	}
	
	/**
	 * Sets the {@link Color} of a part of leather armor
	 * 
	 * @param color the {@link Color} to use
	 * @return this builder for chaining
	 */
	public ItemBuilder color(Color color) {
		if (is.getType() == Material.LEATHER_BOOTS || is.getType() == Material.LEATHER_CHESTPLATE || is.getType() == Material.LEATHER_HELMET
		        || is.getType() == Material.LEATHER_LEGGINGS) {
			LeatherArmorMeta meta = (LeatherArmorMeta) is.getItemMeta();
			meta.setColor(color);
			is.setItemMeta(meta);
			return this;
		} else {
			throw new IllegalArgumentException("color() only applicable for leather armor!");
		}
	}
	
	/**
	 * Sets the {@link owner} of the skull item
	 * 
	 * @param set the {@link owner} of the skull
	 * @return this builder for chaining
	 */
	public ItemBuilder setSkullOwner(String owner) {
		if (is.getType() == Material.SKULL_ITEM || is.getType() == Material.SKULL) {
			is.setDurability((short) SkullType.PLAYER.ordinal());
			SkullMeta meta = (SkullMeta)is.getItemMeta();
			meta.setOwner(owner);
			is.setItemMeta(meta);
			return this;
		} else {
			throw new IllegalArgumentException("skullOwner() only applicable for skulls!");
		}
	}
	
	/**
	 * Builds the {@link ItemStack}
	 * 
	 * @return the created {@link ItemStack}
	 */
	public ItemStack build() {
		return is;
	}
}
