package com.avalon.utils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class FileUtils {
    
    public static File home;
	
	
    
    
    public static void writeToConfig(String config, String path, Object arg) {
    	File configlist = new File(home.getAbsolutePath() + File.separator + config);
    	FileConfiguration cfg = YamlConfiguration.loadConfiguration(configlist);
    	
    	cfg.set(path, arg);
    	try {
			cfg.save(configlist);
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    }
    
    
    public static void updateConfig() {
    	File configlist = new File(home.getAbsolutePath() + File.separator + "config.yml");
    	
    	
		try {
			FileConfiguration cfg = YamlConfiguration
					.loadConfiguration(configlist);
			int u = 0;
			if (!cfg.isSet("main.blockbuyablecommands")) {
				cfg.set("main.blockbuyablecommands", false);
				u++;
			}
			if (!cfg.isSet("main.EnableClickSound")) {
				cfg.set("main.EnableClickSound", true);
				u++;
			}
			if (!cfg.isSet("main.Item_Currency")) {
				cfg.set("main.Item_Currency", "EMERALD");
				u++;
			}
			if (!cfg.isSet("main.EnableMetrics")) {
				cfg.set("main.EnableMetrics", true);
				u++;
			}
			
			if (!cfg.isSet("Vote.Enable")) {
				cfg.set("Vote.Enable", false);
				u++;
			}
			
			if (!cfg.isSet("Vote.TokenAmount")) {
				cfg.set("Vote.TokenAmount", 1);
				u++;
			}
			
			if (cfg.isSet("Item_Currency")) {
				cfg.set("Item_Currency", null);
				u++;
			}
			
			if (!cfg.isSet("main.EnableMySQL")) {
				cfg.set("main.EnableMySQL", false);
				u++;
			}
			
			if (!cfg.isSet("main.EnableToken")) {
				cfg.set("main.EnableToken", true);
				u++;
			}
			
			if (!cfg.isSet("main.EnableSlotPermissions")) {
				cfg.set("main.EnableSlotPermissions", false);
				u++;
			}
			
			if (!cfg.isSet("mysql.Hostname")) {
				cfg.set("mysql.Hostname", "localhost");
				u++;
			}
			if (!cfg.isSet("mysql.Port")) {
				cfg.set("mysql.Port", 3306);
				u++;
			}
			if (!cfg.isSet("mysql.User")) {
				cfg.set("mysql.User", "root");
				u++;
			}
			if (!cfg.isSet("mysql.Password")) {
				cfg.set("mysql.Password", "password");
				u++;
			}
			if (!cfg.isSet("mysql.Database")) {
				cfg.set("mysql.Database", "Minecraft");
				u++;
			}
			
			
			
		    File shopFolder = new File(home.getAbsolutePath() + File.separator + "shops");
		    if (!shopFolder.exists()) {
		    	shopFolder.mkdir();
		    	System.out.println("Created shop folder");
		    	Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[ServerShop-GUI] =========== INFO ===========");
		    	Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Please move all your shop yml files to the subfolder /shops");
		    	Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Please move all your shop yml files to the subfolder /shops");
		    	Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Please move all your shop yml files to the subfolder /shops");
		    	Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Please move all your shop yml files to the subfolder /shops");
		    	Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Please move all your shop yml files to the subfolder /shops");
		    	Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[ServerShop-GUI] =========== INFO ===========");
		    	Bukkit.getConsoleSender().sendMessage(ChatColor.YELLOW + "-This Info is only be displayed once-");
		    }

			if (u != 0)
				Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[ServerShop-GUI] Updated " + u + " lines in your configuration.");
			cfg.save(configlist);
		} catch (IOException ex) {
		}
    }
    
	
	public static void setupMainConfig() {
		
		File configlist = new File(home.getAbsolutePath() + File.separator + "config.yml");
		
		try {
			configlist.createNewFile();
			FileConfiguration cfg = YamlConfiguration.loadConfiguration(configlist);
			cfg.set("main.slots", 27);
			cfg.set("main.enableCommand", true);
			cfg.set("main.enableItem", false);
			cfg.set("main.shopOpenWithItem", "Compass");
			cfg.set("main.blockbuyablecommands", false);
			cfg.set("main.EnableLogs", false);
			cfg.set("main.EnableClickSound", true);
			cfg.set("main.EnableMetrics", true);
			cfg.set("main.Item_Currency", "EMERALD");
			cfg.set("main.Debug", false);
			cfg.set("main.firststart", true);
			cfg.set("main.EnableMySQL", false);
			
			cfg.set("mysql.Hostname", "localhost");
			cfg.set("mysql.Port", "3306");
			cfg.set("mysql.Password", "password");
			cfg.set("mysql.User", "root");
			cfg.set("mysql.Database", "Minecraft");
			
			cfg.set("Vote.Enable", true);
			cfg.set("Vote.TokenAmount", 1);
			
			cfg.set("list.0.size", 27 );
			cfg.set("list.0.name", "Items" );
			cfg.set("list.0.icon", "Wood");
			cfg.set("list.0.description", Arrays.asList("&6This is the new default", "description for your shop", "&6Have fun."));
			cfg.set("list.0.returnbutton", true);
			
			
			
			cfg.save(configlist);
		} catch (IOException ex) {
		}
		
	}
	
	public static void TokenConfig() {
		
		File configlist = new File(home.getAbsolutePath() + File.separator + "token.yml");
		
		try {
			configlist.createNewFile();
			FileConfiguration cfg = YamlConfiguration.loadConfiguration(configlist);
			cfg.save(configlist);
		} catch (IOException ex) {
		}
		
	}
	
	public static void setupItemShopConfig() {
		
		File configlist = new File(home.getAbsolutePath() + File.separator + "config.yml");
		FileConfiguration config = YamlConfiguration.loadConfiguration(configlist);
		if (config.getBoolean("main.firststart")) {
	    System.out.println("Creating shops folder...");
	    File shopFolder = new File(home.getAbsolutePath() + File.separator + "shops");
	    if (!shopFolder.exists()) {
	    	shopFolder.mkdir();
	    	System.out.println("Created Shop Folder");
	    }
		File shop = new File(home.getAbsolutePath() + File.separator + "shops" + File.separator + "Items.yml");
		if (!shop.exists()) {
			try {
				shop.createNewFile();
				FileConfiguration cfg = YamlConfiguration.loadConfiguration(shop);
				
				cfg.set("stock.0.Type", "PlayerCommand");
				cfg.set("stock.0.Reward", "Diamond");
				cfg.set("stock.0.Pricetype", "Money");
				cfg.set("stock.0.Displayname", "Show your money balance");
				cfg.set("stock.0.Command", "/money");
				cfg.set("stock.0.Price", 15);
				
				cfg.set("stock.1.Type", "Item");
				cfg.set("stock.1.Reward", "Wood:1");
				cfg.set("stock.1.Displayname", "This is a custom Item name.");
				cfg.set("stock.1.lore", Arrays.asList("Look a ", "fancy lore", "&ccan &abe &fadded :)"));
				cfg.set("stock.1.Pricetype", "Money");
				cfg.set("stock.1.Price", 25.0);
				cfg.set("stock.1.Sell", 5.0);
				cfg.set("stock.1.Amount", 9);
				
				cfg.set("stock.2.Type", "Item");
				cfg.set("stock.2.Reward", "Stone");
				cfg.set("stock.2.Pricetype", "Money");
				cfg.set("stock.2.Price", 40.0);
				cfg.set("stock.2.Sell", 19.5);
				cfg.set("stock.2.Amount", 9);
				
				cfg.set("stock.3.Type", "Permission");
				cfg.set("stock.3.Reward", "Diamond_Pickaxe");
				cfg.set("stock.3.Permission", "essentials.home:essentials.fly:essentials.warp");
				cfg.set("stock.3.Displayname", "VIP Package");
				cfg.set("stock.3.lore", Arrays.asList("&eThis Upgrade unlocks ", "&ethe usage of the &a/home, /fly and /warp", "&ecommand on your account"));
				cfg.set("stock.3.Pricetype", "Money");
				cfg.set("stock.3.Price", 25.0);
				
				cfg.set("stock.4.Type", "Item");
				cfg.set("stock.4.Reward", "Diamond_sword");
				cfg.set("stock.4.Displayname", "Sword of Fire");
				cfg.set("stock.4.Enchantments", "FIRE_ASPECT:3;KNOCKBACK:40");
				cfg.set("stock.4.Pricetype", "Money");
				cfg.set("stock.4.Price", 25.0);
				cfg.set("stock.4.Sell", -1);
				cfg.set("stock.4.Amount", 1);
				
				cfg.set("stock.5.Type", "Item");
				cfg.set("stock.5.Reward", "Mob_Spawner");
				cfg.set("stock.5.Pricetype", "Money");
				cfg.set("stock.5.Price", 500.125);
				cfg.set("stock.5.Sell", 0.0);
				cfg.set("stock.5.Amount", 1);

				cfg.set("stock.6.Type", "Item");
				cfg.set("stock.6.Reward", "Lever");
				cfg.set("stock.6.Pricetype", "EXP");
				cfg.set("stock.6.Price", 5);
				cfg.set("stock.6.Sell", 1);
				cfg.set("stock.6.Amount", 64);
				
				cfg.set("stock.7.Type", "Item");
				cfg.set("stock.7.Reward", "Apple");
				cfg.set("stock.7.Pricetype", "Money");
				cfg.set("stock.7.Price", 0);
				cfg.set("stock.7.Sell", 0.0);
				cfg.set("stock.7.Amount", 20);
				
				cfg.set("stock.8.Type", "Item");
				cfg.set("stock.8.Reward", "MONSTER_EGG:90");
				cfg.set("stock.8.Pricetype", "EXP");
				cfg.set("stock.8.Price", 15);
				cfg.set("stock.8.Sell", 10);
				cfg.set("stock.8.Amount", 5);
				
				cfg.set("stock.9.Type", "Command");
				cfg.set("stock.9.Reward", "Wood:4");
				cfg.set("stock.9.Pricetype", "Money");
				cfg.set("stock.9.Displayname", "Free Wood");
				cfg.set("stock.9.Command", "give %player% wood");
				cfg.set("stock.9.Price", 15);
				
				cfg.set("stock.10.Type", "Command");
				cfg.set("stock.10.Reward", "Wood:4");
				cfg.set("stock.10.Pricetype", "Money");
				cfg.set("stock.10.Displayname", "Multiple Commands is &cNEW!");
				cfg.set("stock.10.lore", Arrays.asList("&eThis is a new Feature ", "&eIt will run multple commands", "in this case give and a say announce!"));
				cfg.set("stock.10.Command", "give %player% wood:say %player% has just bought the wood package!");
				cfg.set("stock.10.Price", 15);
				
				cfg.set("stock.11.Type", "Command");
				cfg.set("stock.11.Reward", "Wood:4");
				cfg.set("stock.11.Pricetype", "Token");
				cfg.set("stock.11.Displayname", "Wood Reward");
				cfg.set("stock.11.lore", Arrays.asList("&eThis is a new Feature ", "&eIt will run multple commands", "in this case give and a say announce!", "and requires the new Token feature!"));
				cfg.set("stock.11.Command", "give %player% wood 64");
				cfg.set("stock.11.Price", 50);
				config.set("main.firststart", false);
				cfg.save(shop);
				config.save(configlist);
			} catch (IOException ex) {
			}
		}
		} else {
			return;
		}
	}
}
